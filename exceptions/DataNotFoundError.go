package exceptions

type DataNotFoundError struct {
	Message string
}

func (e *DataNotFoundError) Error() string {
	return e.Message
}

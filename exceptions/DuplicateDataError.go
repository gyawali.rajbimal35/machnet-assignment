package exceptions

type DuplicateDataError struct {
	Message string
}

func (e *DuplicateDataError) Error() string {
	return e.Message
}

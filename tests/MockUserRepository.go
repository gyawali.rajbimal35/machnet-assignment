package tests

import (
	"machent-assignment/dto"
	"machent-assignment/model"
)

type MockUserRepository struct {
}

func NewMockUserRepository() *MockUserRepository {
	mur := MockUserRepository{}

	return &mur
}

func (mur *MockUserRepository) Save(u *model.User) error {
	return nil
}

func (mur *MockUserRepository) FindByEmail(email string) (model.User, error) {
	return model.User{}, nil
}

func (mur *MockUserRepository) CreateRelationShipBetween(r *dto.AddToFriendListRequest) error {
	return nil
}

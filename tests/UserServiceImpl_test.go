package tests

import (
	"machent-assignment/dto"
	"machent-assignment/service"
	"testing"
)

var us service.UserServiceImpl = *service.NewUserServiceImpl(NewMockUserRepository())

func TestCreateUserProfile(t *testing.T) {
	response, _ := us.CreateUserProfile(&dto.CreateProfileRequest{
		Email:    "abc@gmail.com",
		FullName: "ABC",
		PhoneNo:  "98287388222",
		Dob:      "1992-01-03",
	})

	if response == nil {
		t.Errorf("Expected %q, Got %q", response.Message, "nil")
		return
	}

	t.Log("TestCreateUserProfile passed")

}

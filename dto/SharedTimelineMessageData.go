package dto

type SharedTimelineMessageData struct {
	Id        string `json:"id"`
	Body      string `json:"body"`
	CreatedOn string `json:"createdOn"`
	PostedBy  string `json:"postedBy"`
	SharedBy  string `json:"sharedBy"`
}

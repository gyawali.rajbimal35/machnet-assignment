package dto

type UserData struct {
	FullName string `json:"fullName"`
	Email    string `json:"email"`
}

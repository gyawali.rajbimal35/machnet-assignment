package dto

type CreateTimelineMessageResponse struct {
	Id        string `json:"id"`
	CreatedOn string `json:"createdOn"`
}

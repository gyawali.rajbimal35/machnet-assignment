package dto

type CreateTimelineMessageRequest struct {
	Body     string `json:"body"`
	PostedBy string `json:"postedBy"`
}

package dto

type CreateProfileRequest struct {
	FullName string `json:"fullName"`
	Email    string `json:"email"`
	PhoneNo  string `json:"phoneNo"`
	Dob      string `json:"dob"`
}

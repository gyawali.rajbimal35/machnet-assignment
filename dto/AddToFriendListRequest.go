package dto

type AddToFriendListRequest struct {
	SourceUserEmail string `json:"sourceUserEmail"`
	TargetUserEmail string `json:"targetUserEmail"`
}

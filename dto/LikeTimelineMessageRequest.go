package dto

type LikeTimelineMessageRequest struct {
	LikerEmail        string `json:"likerEmail"`
	TimelineMessageId string `json:"timelineMessageId"`
}

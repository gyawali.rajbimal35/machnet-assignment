package dto

type ShareTimelineMessageRequest struct {
	SharerEmail       string `json:"sharerEmail"`
	TimelineMessageId string `json:"timelineMessageId"`
}

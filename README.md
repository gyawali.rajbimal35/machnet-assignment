### Machnet Assignment


## Introduction 
This repository contains solution to Machnet assignment.

The solution contains REST APIs implemented using **Controller-Service-Repository** Pattern.

Technologies used for the assignment :

- GoLang - Programming Language
- Gin - Web Framework
- Neo4j - Graph Database


## Database

The solution uses **Neo4j**, a Graph Database.

The following image shows the database schema of the solution.

![alt neo4jdb](/docs/machnet-db.png)



## Application Properties

Database properties are kept at [Neo4jProperties.go](https://gitlab.com/gyawali.rajbimal35/machnet-assignment/-/blob/develop/properties/Neo4jProperites.go)

Server Properties are kept at [ServerProperties.go](https://gitlab.com/gyawali.rajbimal35/machnet-assignment/-/blob/develop/properties/ServerProperties.go)

## API Documentation

The API documentation is published at :
[https://documenter.getpostman.com/view/8469089/2s8YmSrg8j](https://documenter.getpostman.com/view/8469089/2s8YmSrg8j)

*Preview of documentation* 

![alt postman](/docs/postman.png)


## My Social Profile

Github: https://github.com/BimalRajGyawali
<br>
Linkedin: https://www.linkedin.com/in/bimal-raj-gyawali-238a50117/



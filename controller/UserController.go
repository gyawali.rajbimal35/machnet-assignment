package controller

import (
	"log"
	"machent-assignment/dto"
	"machent-assignment/exceptions"
	"machent-assignment/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UserController struct {
	us service.UserService
}

func NewUserController(us service.UserService) *UserController {
	uc := UserController{us: us}

	return &uc
}

func (uc *UserController) CreateProfile(c *gin.Context) {
	var request dto.CreateProfileRequest

	if err := c.BindJSON(&request); err != nil {
		return
	}

	response, err := uc.us.CreateUserProfile(&request)

	if err != nil {
		e, ok := err.(*exceptions.DuplicateDataError)
		if ok {
			c.JSON(http.StatusBadRequest, map[string]string{"error": e.Message})
		} else {
			c.JSON(http.StatusInternalServerError, map[string]string{"error": "Something went wrong"})
		}
		return
	}

	c.JSON(http.StatusCreated, response)

}

func (uc *UserController) AddToFriendList(c *gin.Context) {
	var request dto.AddToFriendListRequest

	if err := c.BindJSON(&request); err != nil {
		log.Println(err)
		return
	}

	err := uc.us.AddToFriendList(&request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]string{"error": "Something went wrong"})
		return
	}

	c.JSON(http.StatusCreated,
		map[string]string{"message": "You have added " + request.TargetUserEmail + " as a friend"})

}

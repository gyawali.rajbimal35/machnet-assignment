package controller

import (
	"log"
	"machent-assignment/dto"
	"machent-assignment/exceptions"
	"machent-assignment/service"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

type TimelineMessageController struct {
	tms service.TimelineMessageService
}

func NewTimelineMessageController(tms service.TimelineMessageService) *TimelineMessageController {
	tmc := TimelineMessageController{tms: tms}

	return &tmc
}

func (tmc *TimelineMessageController) PostMessageOnTimeline(c *gin.Context) {
	var request dto.CreateTimelineMessageRequest

	if err := c.BindJSON(&request); err != nil {
		return
	}

	timelineMessageResponse, err := tmc.tms.PostMessageOnTimeline(&request)

	if err != nil {
		e, ok := err.(*exceptions.DataNotFoundError)
		if ok {
			c.JSON(http.StatusNotFound, map[string]string{"error": e.Message})
		} else {
			c.JSON(http.StatusInternalServerError, map[string]string{"error": "Something went wrong"})
		}

		return
	}

	c.JSON(http.StatusCreated, timelineMessageResponse)

}

func (tmc *TimelineMessageController) ListTimelineMessagesOfFriends(c *gin.Context) {

	email, ok := c.GetQuery("email")

	if !ok || email == "" {
		//also check if email is a valid email
		c.JSON(http.StatusBadRequest, map[string]string{"message": "Bad Request"})
		return
	}

	data, err := tmc.tms.ListFriendsTimelineMessages(email)

	if err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, map[string]string{"message": "Something went wrong"})
		return
	}

	c.JSON(http.StatusOK, data)
}

func (tmc *TimelineMessageController) LikeTimelineMessage(c *gin.Context) {
	var request dto.LikeTimelineMessageRequest

	if err := c.BindJSON(&request); err != nil {
		log.Println(err)
		return
	}

	if strings.TrimSpace(request.LikerEmail) == "" ||
		strings.TrimSpace(request.TimelineMessageId) == "" {
		c.JSON(http.StatusBadRequest, map[string]string{"error": "Fields cannot be empty"})
		return
	}

	err := tmc.tms.LikeTimelineMessage(&request)

	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]string{"error": "Something went wrong"})
		return
	}

	c.JSON(http.StatusCreated,
		map[string]string{"message": "Timeline Message liked successfully"})
}

func (tmc *TimelineMessageController) ShareTimelineMessage(c *gin.Context) {
	var request dto.ShareTimelineMessageRequest

	if err := c.BindJSON(&request); err != nil {
		log.Println(err)
		return
	}

	if strings.TrimSpace(request.SharerEmail) == "" ||
		strings.TrimSpace(request.TimelineMessageId) == "" {
		c.JSON(http.StatusBadRequest, map[string]string{"error": "Fields cannot be empty"})
		return
	}

	err := tmc.tms.ShareTimelineMessage(&request)

	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]string{"error": "Something went wrong"})
		return
	}

	c.JSON(http.StatusCreated,
		map[string]string{"message": "Timeline Message Shared successfully"})
}

func (tmc *TimelineMessageController) GetLikersOfTimelineMessage(c *gin.Context) {

	timelineMessageId, ok := c.GetQuery("timelineMessageId")

	if !ok || timelineMessageId == "" {
		c.JSON(http.StatusBadRequest, map[string]string{"message": "Bad Request"})
		return
	}

	data, err := tmc.tms.GetLikersOfTimelineMessage(timelineMessageId)

	if err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, map[string]string{"message": "Something went wrong"})
		return
	}

	c.JSON(http.StatusOK, data)
}

func (tmc *TimelineMessageController) GetSharersOfTimelineMessage(c *gin.Context) {

	timelineMessageId, ok := c.GetQuery("timelineMessageId")

	if !ok || timelineMessageId == "" {
		c.JSON(http.StatusBadRequest, map[string]string{"message": "Bad Request"})
		return
	}

	data, err := tmc.tms.GetSharersOfTimelineMessage(timelineMessageId)

	if err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, map[string]string{"message": "Something went wrong"})
		return
	}

	c.JSON(http.StatusOK, data)
}

func (tmc *TimelineMessageController) GetLikedTimelineMessagesByUser(c *gin.Context) {
	email, ok := c.GetQuery("email")

	if !ok || email == "" {
		c.JSON(http.StatusBadRequest, map[string]string{"message": "Bad Request"})
		return
	}
	data, err := tmc.tms.GetLikedTimelineMessagesByUser(email)

	if err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, map[string]string{"message": "Something went wrong"})
		return
	}
	c.JSON(http.StatusOK, data)

}

func (tmc *TimelineMessageController) GetSharedTimelineMessagesByUser(c *gin.Context) {
	email, ok := c.GetQuery("email")

	if !ok || email == "" {
		c.JSON(http.StatusBadRequest, map[string]string{"message": "Bad Request"})
		return
	}
	data, err := tmc.tms.GetSharedTimelineMessagesByUser(email)

	if err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, map[string]string{"message": "Something went wrong"})
		return
	}
	c.JSON(http.StatusOK, data)

}

func (tmc *TimelineMessageController) GetTimelineMessagesSharedByFriends(c *gin.Context) {

	email, ok := c.GetQuery("email")

	if !ok || email == "" {
		c.JSON(http.StatusBadRequest, map[string]string{"message": "Bad Request"})
		return
	}

	data, err := tmc.tms.GetTimelineMessagesSharedByFriends(email)

	if err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, map[string]string{"message": "Something went wrong"})
		return
	}

	c.JSON(http.StatusOK, data)
}

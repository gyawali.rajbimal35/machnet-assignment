package repository

import (
	"context"
	"log"
	"machent-assignment/dto"
	"machent-assignment/model"

	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
)

type TimelineMessageRepositoryImpl struct {
	ctx    context.Context
	driver neo4j.DriverWithContext
}

func NewTimeLineMessageRepositoryImpl(ctx context.Context,
	driver neo4j.DriverWithContext) *TimelineMessageRepositoryImpl {

	tmr := TimelineMessageRepositoryImpl{ctx: ctx, driver: driver}

	return &tmr
}

func (tmr *TimelineMessageRepositoryImpl) Save(tm *model.TimelineMessage) error {

	session := tmr.driver.NewSession(tmr.ctx, neo4j.SessionConfig{})

	defer session.Close(tmr.ctx)

	_, err := session.ExecuteWrite(tmr.ctx, func(tx neo4j.ManagedTransaction) (any, error) {

		_, err := tx.Run(tmr.ctx, `CREATE (m:TimelineMessage { Id: $Id, Body: $Body, 
			PostedBy: $PostedBy, CreatedOn: $CreatedOn }) `,
			map[string]any{

				"Id":        tm.Id,
				"Body":      tm.Body,
				"PostedBy":  tm.PostedBy,
				"CreatedOn": tm.CreatedOn,
			})

		if err != nil {
			log.Println(err)
			return nil, err
		}

		_, errr := tx.Run(tmr.ctx, `MATCH (u:User), (m:TimelineMessage) WHERE u.Email = $PostedBy 
		AND m.Id = $TimelineMessageId
		CREATE (u)-[r:ADD_NEW]->(m)`,
			map[string]any{
				"PostedBy":          tm.PostedBy,
				"TimelineMessageId": tm.Id,
			})

		return nil, errr
	})

	return err
}

func (tmr *TimelineMessageRepositoryImpl) GetAllTimelineMessageOfFriends(email string) ([]dto.TimelineMessageData, error) {
	session := tmr.driver.NewSession(tmr.ctx, neo4j.SessionConfig{})

	defer session.Close(tmr.ctx)

	timelineMessages, err := session.ExecuteRead(tmr.ctx, func(tx neo4j.ManagedTransaction) (any, error) {

		var timelineMessages []dto.TimelineMessageData = []dto.TimelineMessageData{}

		result, err := tx.Run(tmr.ctx, ` MATCH p = (u:User {Email: $Email})-[:IS_FRIEND] 
		->(:User)-[:ADD_NEW] -> (t:TimelineMessage)

		RETURN t.Id, t.Body, t.PostedBy, t.CreatedOn
		`,
			map[string]any{
				"Email": email,
			})

		if err != nil {
			log.Println(err)
			return nil, err
		}
		for result.Next(tmr.ctx) {

			v := result.Record().Values

			if v[0] == nil {
				return nil, nil
			}
			timeLineMessage := dto.TimelineMessageData{
				Id:        v[0].(string),
				Body:      v[1].(string),
				PostedBy:  v[2].(string),
				CreatedOn: v[3].(string),
			}

			timelineMessages = append(timelineMessages, timeLineMessage)
		}

		log.Println(timelineMessages)
		return timelineMessages, nil

	})

	return timelineMessages.([]dto.TimelineMessageData), err
}

func (tmr *TimelineMessageRepositoryImpl) GetTimelineMessagesSharedByFriends(email string) ([]dto.SharedTimelineMessageData, error) {
	session := tmr.driver.NewSession(tmr.ctx, neo4j.SessionConfig{})

	defer session.Close(tmr.ctx)

	timelineMessages, err := session.ExecuteRead(tmr.ctx, func(tx neo4j.ManagedTransaction) (any, error) {

		var timelineMessages []dto.SharedTimelineMessageData = []dto.SharedTimelineMessageData{}

		result, err := tx.Run(tmr.ctx, ` MATCH p = (u:User {Email: $Email})-[:IS_FRIEND] 
		->(f:User)-[:SHARE] -> (t:TimelineMessage)

		RETURN t.Id, t.Body, t.PostedBy, f.Email AS SharedBy, t.CreatedOn
		`,
			map[string]any{
				"Email": email,
			})

		if err != nil {
			log.Println(err)
			return nil, err
		}
		for result.Next(tmr.ctx) {

			v := result.Record().Values

			if v[0] == nil {
				return nil, nil
			}
			timeLineMessage := dto.SharedTimelineMessageData{
				Id:        v[0].(string),
				Body:      v[1].(string),
				PostedBy:  v[2].(string),
				SharedBy:  v[3].(string),
				CreatedOn: v[4].(string),
			}

			timelineMessages = append(timelineMessages, timeLineMessage)
		}

		log.Println(timelineMessages)
		return timelineMessages, nil

	})

	return timelineMessages.([]dto.SharedTimelineMessageData), err
}

func (tmr *TimelineMessageRepositoryImpl) CreateLikeRelationShipBetween(r *dto.LikeTimelineMessageRequest) error {
	session := tmr.driver.NewSession(tmr.ctx, neo4j.SessionConfig{})

	defer session.Close(tmr.ctx)

	_, err := session.ExecuteWrite(tmr.ctx, func(tx neo4j.ManagedTransaction) (any, error) {
		_, err := tx.Run(tmr.ctx, `MATCH (u:User), (t:TimelineMessage) WHERE u.Email = $LikerEmail AND t.Id = $TimelineMessageId
	  								CREATE (u)-[r:LIKE]->(t)`,
			map[string]any{
				"LikerEmail":        r.LikerEmail,
				"TimelineMessageId": r.TimelineMessageId,
			})

		if err != nil {
			return nil, err
		}
		return nil, nil
	})

	return err
}

func (tmr *TimelineMessageRepositoryImpl) CreateShareRelationShipBetween(r *dto.ShareTimelineMessageRequest) error {
	session := tmr.driver.NewSession(tmr.ctx, neo4j.SessionConfig{})

	defer session.Close(tmr.ctx)

	_, err := session.ExecuteWrite(tmr.ctx, func(tx neo4j.ManagedTransaction) (any, error) {
		_, err := tx.Run(tmr.ctx, `MATCH (u:User), (t:TimelineMessage) WHERE u.Email = $SharerEmail AND t.Id = $TimelineMessageId
	  								CREATE (u)-[r:SHARE]->(t)`,
			map[string]any{
				"SharerEmail":       r.SharerEmail,
				"TimelineMessageId": r.TimelineMessageId,
			})

		if err != nil {
			return nil, err
		}
		return nil, nil
	})

	return err
}

func (tmr *TimelineMessageRepositoryImpl) GetLikersOfTimelineMessage(timelineMessageId string) ([]dto.UserData, error) {

	session := tmr.driver.NewSession(tmr.ctx, neo4j.SessionConfig{})

	defer session.Close(tmr.ctx)

	users, err := session.ExecuteRead(tmr.ctx, func(tx neo4j.ManagedTransaction) (any, error) {

		var users []dto.UserData = []dto.UserData{}

		result, err := tx.Run(tmr.ctx, ` MATCH p = (u:User) -[:LIKE]->(t:TimelineMessage {Id:$timelineMessageId})
		RETURN  u.FullName, u.Email
		`,
			map[string]any{
				"timelineMessageId": timelineMessageId,
			})

		if err != nil {
			log.Println(err)
			return nil, err
		}
		for result.Next(tmr.ctx) {

			v := result.Record().Values

			if v[0] == nil {
				return []dto.UserData{}, nil
			}
			user := dto.UserData{
				FullName: v[0].(string),
				Email:    v[1].(string),
			}

			users = append(users, user)
		}

		return users, nil

	})

	return users.([]dto.UserData), err

}

func (tmr *TimelineMessageRepositoryImpl) GetSharersOfTimelineMessage(timelineMessageId string) ([]dto.UserData, error) {

	session := tmr.driver.NewSession(tmr.ctx, neo4j.SessionConfig{})

	defer session.Close(tmr.ctx)

	users, err := session.ExecuteRead(tmr.ctx, func(tx neo4j.ManagedTransaction) (any, error) {

		var users []dto.UserData = []dto.UserData{}

		result, err := tx.Run(tmr.ctx, ` MATCH p = (u:User) -[:SHARE]->(t:TimelineMessage {Id:$timelineMessageId})
		RETURN u.FullName, u.Email
		`,
			map[string]any{
				"timelineMessageId": timelineMessageId,
			})

		if err != nil {
			log.Println(err)
			return nil, err
		}
		for result.Next(tmr.ctx) {

			v := result.Record().Values

			if v[0] == nil {
				return []dto.UserData{}, nil
			}
			user := dto.UserData{
				FullName: v[0].(string),
				Email:    v[1].(string),
			}

			users = append(users, user)
		}

		return users, nil

	})

	return users.([]dto.UserData), err

}

func (tmr *TimelineMessageRepositoryImpl) GetLikedTimelineMessagesByUser(email string) ([]dto.TimelineMessageData, error) {

	session := tmr.driver.NewSession(tmr.ctx, neo4j.SessionConfig{})

	defer session.Close(tmr.ctx)

	timelineMessages, err := session.ExecuteRead(tmr.ctx, func(tx neo4j.ManagedTransaction) (any, error) {

		var timelineMessages []dto.TimelineMessageData = []dto.TimelineMessageData{}

		result, err := tx.Run(tmr.ctx, ` MATCH p = (u:User {Email: $Email}) -[:LIKE]->(t:TimelineMessage)
		RETURN t.Id, t.Body, t.CreatedOn, t.PostedBy`,

			map[string]any{
				"Email": email,
			})

		if err != nil {
			log.Println(err)
			return nil, err
		}
		for result.Next(tmr.ctx) {

			v := result.Record().Values

			if v[0] == nil {
				return []dto.TimelineMessageData{}, nil
			}
			timelineMessage := dto.TimelineMessageData{
				Id:        v[0].(string),
				Body:      v[1].(string),
				CreatedOn: v[2].(string),
				PostedBy:  v[3].(string),
			}

			timelineMessages = append(timelineMessages, timelineMessage)
		}

		return timelineMessages, nil

	})

	return timelineMessages.([]dto.TimelineMessageData), err

}

func (tmr *TimelineMessageRepositoryImpl) GetSharedTimelineMessagesByUser(email string) ([]dto.TimelineMessageData, error) {

	session := tmr.driver.NewSession(tmr.ctx, neo4j.SessionConfig{})

	defer session.Close(tmr.ctx)

	timelineMessages, err := session.ExecuteRead(tmr.ctx, func(tx neo4j.ManagedTransaction) (any, error) {

		var timelineMessages []dto.TimelineMessageData = []dto.TimelineMessageData{}

		result, err := tx.Run(tmr.ctx, ` MATCH p = (u:User {Email: $Email}) -[:SHARE]->(t:TimelineMessage)
		RETURN t.Id, t.Body, t.CreatedOn, t.PostedBy`,

			map[string]any{
				"Email": email,
			})

		if err != nil {
			log.Println(err)
			return nil, err
		}
		for result.Next(tmr.ctx) {

			v := result.Record().Values

			timelineMessage := dto.TimelineMessageData{
				Id:        v[0].(string),
				Body:      v[1].(string),
				CreatedOn: v[2].(string),
				PostedBy:  v[3].(string),
			}

			timelineMessages = append(timelineMessages, timelineMessage)
		}

		return timelineMessages, nil

	})

	return timelineMessages.([]dto.TimelineMessageData), err

}

package repository

import (
	"machent-assignment/dto"
	"machent-assignment/model"
)

type UserRepository interface {
	Save(u *model.User) error

	FindByEmail(email string) (model.User, error)

	CreateRelationShipBetween(r *dto.AddToFriendListRequest) error
}

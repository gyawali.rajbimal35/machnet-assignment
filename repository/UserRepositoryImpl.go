package repository

import (
	"context"
	"machent-assignment/dto"
	"machent-assignment/model"

	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
)

type UserRepositoryImpl struct {
	ctx    context.Context
	driver neo4j.DriverWithContext
}

func NewUserRepositoryImpl(ctx context.Context, driver neo4j.DriverWithContext) *UserRepositoryImpl {

	ur := UserRepositoryImpl{ctx: ctx, driver: driver}

	return &ur

}

func (ur *UserRepositoryImpl) Save(u *model.User) error {

	session := ur.driver.NewSession(ur.ctx, neo4j.SessionConfig{})

	defer session.Close(ur.ctx)

	_, err := session.ExecuteWrite(ur.ctx, func(tx neo4j.ManagedTransaction) (any, error) {

		_, err := tx.Run(ur.ctx, `CREATE (u:User {FullName: $FullName, 
			PhoneNo: $PhoneNo, Email: $Email, Dob: $Dob }) `, map[string]any{

			"FullName": u.FullName,
			"PhoneNo":  u.PhoneNo,
			"Email":    u.Email,
			"Dob":      u.Dob,
		})

		return nil, err
	})

	return err

}

func (ur *UserRepositoryImpl) FindByEmail(email string) (model.User, error) {
	session := ur.driver.NewSession(ur.ctx, neo4j.SessionConfig{})

	defer session.Close(ur.ctx)

	record, err := session.ExecuteRead(ur.ctx, func(tx neo4j.ManagedTransaction) (any, error) {

		var user model.User

		result, err := tx.Run(ur.ctx, `MATCH (u:User {Email: $email})
		 RETURN u.FullName as FullName, u.Email as Email, u.Dob as Dob, u.PhoneNo as PhoneNo`,
			map[string]any{
				"email": email,
			})

		if err != nil {
			return nil, err
		}

		for result.Next(ur.ctx) {

			v := result.Record().Values

			if v[0] == nil {
				return nil, nil
			}
			user = model.User{
				FullName: v[0].(string),
				Email:    v[1].(string),
				Dob:      v[2].(string),
				PhoneNo:  v[3].(string),
			}
		}

		return user, nil

	})
	if err != nil {
		return model.User{}, err
	}

	return record.(model.User), err

}

func (ur *UserRepositoryImpl) CreateRelationShipBetween(r *dto.AddToFriendListRequest) error {
	session := ur.driver.NewSession(ur.ctx, neo4j.SessionConfig{})

	defer session.Close(ur.ctx)

	_, err := session.ExecuteWrite(ur.ctx, func(tx neo4j.ManagedTransaction) (any, error) {
		_, err := tx.Run(ur.ctx, `MATCH (a:User), (b:User) WHERE a.Email = $SourceUserEmail AND b.Email = $TargetUserEmail
	  								CREATE (a)-[r:IS_FRIEND]->(b)`,
			map[string]any{
				"SourceUserEmail": r.SourceUserEmail,
				"TargetUserEmail": r.TargetUserEmail,
			})

		if err != nil {
			return nil, err
		}

		return nil, nil

	})

	return err
}

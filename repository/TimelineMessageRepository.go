package repository

import (
	"machent-assignment/dto"
	"machent-assignment/model"
)

type TimelineMessageRepository interface {
	Save(tm *model.TimelineMessage) error

	GetAllTimelineMessageOfFriends(email string) ([]dto.TimelineMessageData, error)

	GetTimelineMessagesSharedByFriends(email string) ([]dto.SharedTimelineMessageData, error)

	CreateLikeRelationShipBetween(r *dto.LikeTimelineMessageRequest) error

	CreateShareRelationShipBetween(r *dto.ShareTimelineMessageRequest) error

	GetLikersOfTimelineMessage(timelineMessageId string) ([]dto.UserData, error)

	GetSharersOfTimelineMessage(timelineMessageId string) ([]dto.UserData, error)

	GetLikedTimelineMessagesByUser(email string) ([]dto.TimelineMessageData, error)

	GetSharedTimelineMessagesByUser(email string) ([]dto.TimelineMessageData, error)
}

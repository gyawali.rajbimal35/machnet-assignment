package model

type User struct {
	FullName string
	PhoneNo  string
	Email    string
	Dob      string
}

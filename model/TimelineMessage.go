package model

type TimelineMessage struct {
	Id        string
	Body      string
	CreatedOn string
	PostedBy  string
}

package service

import (
	"log"
	"machent-assignment/dto"
	"machent-assignment/exceptions"
	"machent-assignment/model"
	"machent-assignment/repository"
)

type UserServiceImpl struct {
	ur repository.UserRepository
}

func NewUserServiceImpl(ur repository.UserRepository) *UserServiceImpl {
	us := UserServiceImpl{ur: ur}

	return &us
}

func (us *UserServiceImpl) CreateUserProfile(r *dto.CreateProfileRequest) (*dto.CreateProfileResponse, error) {

	persistedUser, e := us.ur.FindByEmail(r.Email)

	if e != nil {
		log.Panic(e)
		return nil, e
	}

	log.Println(persistedUser)

	if persistedUser.Email != "" {

		return nil, &exceptions.DuplicateDataError{Message: "User already exists"}
	}

	userToBeSaved := model.User{
		FullName: r.FullName,
		Email:    r.Email,
		PhoneNo:  r.PhoneNo,
		Dob:      r.Dob,
	}
	err := us.ur.Save(&userToBeSaved)

	if err != nil {
		log.Panic(err)
		return nil, err
	}

	response := dto.CreateProfileResponse{Message: "Profile created successfully"}

	return &response, nil
}

func (us *UserServiceImpl) AddToFriendList(r *dto.AddToFriendListRequest) error {
	return us.ur.CreateRelationShipBetween(r)
}

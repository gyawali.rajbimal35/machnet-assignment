package service

import "machent-assignment/dto"

type UserService interface {
	CreateUserProfile(r *dto.CreateProfileRequest) (*dto.CreateProfileResponse, error)

	AddToFriendList(r *dto.AddToFriendListRequest) error
}

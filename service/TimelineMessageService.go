package service

import (
	"machent-assignment/dto"
)

type TimelineMessageService interface {
	PostMessageOnTimeline(r *dto.CreateTimelineMessageRequest) (*dto.CreateTimelineMessageResponse, error)

	ListFriendsTimelineMessages(email string) ([]dto.TimelineMessageData, error)

	GetTimelineMessagesSharedByFriends(email string) ([]dto.SharedTimelineMessageData, error)

	LikeTimelineMessage(r *dto.LikeTimelineMessageRequest) error

	ShareTimelineMessage(r *dto.ShareTimelineMessageRequest) error

	GetLikersOfTimelineMessage(timelineMessageId string) ([]dto.UserData, error)

	GetSharersOfTimelineMessage(timelineMessageId string) ([]dto.UserData, error)

	GetLikedTimelineMessagesByUser(email string) ([]dto.TimelineMessageData, error)

	GetSharedTimelineMessagesByUser(email string) ([]dto.TimelineMessageData, error)
}

package service

import (
	"machent-assignment/dto"
	"machent-assignment/exceptions"
	"machent-assignment/model"
	"machent-assignment/repository"
	"time"

	"github.com/google/uuid"
)

type TimelineMessageServiceImpl struct {
	tmr repository.TimelineMessageRepository
	ur  repository.UserRepository
}

func NewTimelineMessageServiceImpl(tmr repository.TimelineMessageRepository,
	ur repository.UserRepository) *TimelineMessageServiceImpl {
	tms := TimelineMessageServiceImpl{tmr: tmr, ur: ur}

	return &tms
}

func (tms *TimelineMessageServiceImpl) PostMessageOnTimeline(r *dto.CreateTimelineMessageRequest) (*dto.CreateTimelineMessageResponse, error) {
	//check if UserEmail exists
	persistedUser, err := tms.ur.FindByEmail(r.PostedBy)

	if err != nil {
		return nil, err
	}
	if persistedUser.Email == "" {
		return nil, &exceptions.DataNotFoundError{Message: "User with given email not found"}
	}

	timelineMessage := model.TimelineMessage{
		Id:        uuid.New().String(),
		Body:      r.Body,
		PostedBy:  r.PostedBy,
		CreatedOn: time.Now().String(),
	}
	er := tms.tmr.Save(&timelineMessage)

	if er != nil {
		return nil, er
	}

	return &dto.CreateTimelineMessageResponse{
		Id:        timelineMessage.Id,
		CreatedOn: timelineMessage.CreatedOn,
	}, nil

}

func (tms *TimelineMessageServiceImpl) ListFriendsTimelineMessages(email string) ([]dto.TimelineMessageData, error) {
	return tms.tmr.GetAllTimelineMessageOfFriends(email)
}

func (tms *TimelineMessageServiceImpl) GetTimelineMessagesSharedByFriends(email string) ([]dto.SharedTimelineMessageData, error) {
	return tms.tmr.GetTimelineMessagesSharedByFriends(email)
}

func (tms *TimelineMessageServiceImpl) LikeTimelineMessage(r *dto.LikeTimelineMessageRequest) error {
	return tms.tmr.CreateLikeRelationShipBetween(r)
}

func (tms *TimelineMessageServiceImpl) ShareTimelineMessage(r *dto.ShareTimelineMessageRequest) error {
	return tms.tmr.CreateShareRelationShipBetween(r)
}

func (tms *TimelineMessageServiceImpl) GetLikersOfTimelineMessage(timelineMessageId string) ([]dto.UserData, error) {
	return tms.tmr.GetLikersOfTimelineMessage(timelineMessageId)
}

func (tms *TimelineMessageServiceImpl) GetSharersOfTimelineMessage(timelineMessageId string) ([]dto.UserData, error) {
	return tms.tmr.GetSharersOfTimelineMessage(timelineMessageId)
}

func (tms *TimelineMessageServiceImpl) GetLikedTimelineMessagesByUser(email string) ([]dto.TimelineMessageData, error) {
	return tms.tmr.GetLikedTimelineMessagesByUser(email)
}

func (tms *TimelineMessageServiceImpl) GetSharedTimelineMessagesByUser(email string) ([]dto.TimelineMessageData, error) {
	return tms.tmr.GetSharedTimelineMessagesByUser(email)
}

package main

import (
	"context"
	"log"
	"machent-assignment/controller"
	"machent-assignment/properties"
	"machent-assignment/repository"
	"machent-assignment/service"

	"github.com/gin-gonic/gin"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
)

func main() {

	r := gin.Default()

	dbUri := properties.DB_URL
	driver, err := neo4j.NewDriverWithContext(dbUri, neo4j.BasicAuth(properties.DB_USER, properties.DB_PASSWORD, ""))
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	defer driver.Close(ctx)

	ur := repository.NewUserRepositoryImpl(ctx, driver)
	us := service.NewUserServiceImpl(ur)
	uc := controller.NewUserController(us)

	tmr := repository.NewTimeLineMessageRepositoryImpl(ctx, driver)
	tms := service.NewTimelineMessageServiceImpl(tmr, ur)
	tmc := controller.NewTimelineMessageController(tms)

	log.Println("Starting server...")

	r.POST("/api/v1/users", uc.CreateProfile)
	r.POST("/api/v1/friends", uc.AddToFriendList)

	r.POST("/api/v1/timeline-messages", tmc.PostMessageOnTimeline)

	r.GET("/api/v1/friends-timeline-messages", tmc.ListTimelineMessagesOfFriends)
	r.GET("/api/v1/friends-shared-timeline-messages", tmc.GetTimelineMessagesSharedByFriends)

	r.POST("/api/v1/likes", tmc.LikeTimelineMessage)
	r.POST("/api/v1/shares", tmc.ShareTimelineMessage)

	r.GET("/api/v1/likers", tmc.GetLikersOfTimelineMessage)
	r.GET("/api/v1/sharers", tmc.GetSharersOfTimelineMessage)

	r.GET("/api/v1/liked-timeline-messages", tmc.GetLikedTimelineMessagesByUser)
	r.GET("/api/v1/shared-timeline-messages", tmc.GetSharedTimelineMessagesByUser)

	r.Run(properties.SERVER_URL)

}
